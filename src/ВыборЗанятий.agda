module ВыборЗанятий where

open import ИнтуиционистскаяЛогика
open import Равенство
open import Список
open import НатуральноеЧисло
open import МножествоОтношение


module ПолезныеФункцииНаОтношениях where

  все : {А : Set} → ℙ₀ А → ℙ₀ (Список А)
  все п [] = ⊤
  все п (з ∷ тд) = п з × все п тд

  _¿ : {А : Set} → ℙ₀ А → А ★ А
  (п ¿) а б = (а ≡ б) × п а

open ПолезныеФункцииНаОтношениях


Занятие : Set
Занятие = Σ (ℕ × ℕ) (λ отр → Σ-проекция₁ отр < Σ-проекция₂ отр)

старт : Занятие → ℕ
старт з = Σ-проекция₁ (Σ-проекция₁ з)

финиш : Занятие → ℕ
финиш з = Σ-проекция₂ (Σ-проекция₁ з)

непересек : Занятие ★ Занятие
непересек а б = (финиш б ≤ старт а) ⊎ (финиш а ≤ старт б)

совместны : ℙ₀ (Занятие × Список Занятие)
совместны (з , тд) = все (непересек з) тд
